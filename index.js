var Elements = document.getElementsByClassName("inputElement");

function BindButtons (){
    for (let index = 0; index < Elements.length; index++) {
        const inputElement = Elements[index];
        inputElement.addEventListener("click", e => {
            OnClick(e.target.name,inputElement.checked);
        });
    }
}

function OnLoad () {
    for(let x of Elements){
        let value = localStorage.getItem(x.name);
        let  bool = (value == "true") ? true : false;
        x.checked = bool;
    }
}

function OnClick (name,value){
    localStorage.setItem(name,value);
}

OnLoad();
BindButtons();